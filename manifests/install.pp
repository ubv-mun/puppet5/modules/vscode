# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include vscode::install
class vscode::install {

  if $facts['os']['architecture'] == 'amd64' {

    case $vscode::package_ensure {
      'present', 'installed', 'latest', 'held': {
        include '::apt'

        create_resources('::apt::source', $vscode::sources)
        Class['apt::update'] -> Package <| |>
      }
    }

    package { $vscode::package_name:
      ensure => $vscode::package_ensure,
    }
  }
}
