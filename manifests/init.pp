# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include vscode
class vscode (
  String        $package_ensure,
  Array[String] $package_name,
  Hash          $sources,
) {
  contain 'vscode::install'
}
